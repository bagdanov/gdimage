# Image generation from Gaussian derivative basis

To create the Anaconda environment:

```
conda create --name <env> --file requirements.txt
```

Then run the `gdImage.ipynb` notebook in Jupyter.
